# How to Setup Client VPN Connection

## Overview

**AWS Client VPN** is a managed client-based VPN service that enables you to securely access your AWS resources and resources in your on-premises network. With Client VPN, you can access your resources from any location using an OpenVPN-based VPN client. The difference between **AWS Site-to-Site VPN** and **AWS Client VPN** is that terminate endpoint of Client VPN is at client site but endpoint of Site-to-Site VPN is at AWS end (aka AWS managed VPN). AWS Client VPN is available in US East (Virginia), US East (Ohio), US West (Oregon) and EU (Ireland) AWS Regions. 

## Scenario

To complete this tutorial, below steps you need the following: 
- **1. Generate Server and Client Certificates and Keys**: We use 
existed **aws-self-signed-acm.sh** as authorization. With this, client VPN uses certificates to perform authentication between the client and the sever. 
- **2. Create a Client VPN Endpoint**: When you create a Client VPN endpoint, you create the VPN construct to which clients can connect to establish a VPN connection. 
- **3. Enable VPN Connectivity for Clients**: To enable clients to establish a VPN session, you must associate a target network with the Client VPN endpoint. A target network is a subnet in a VPC. 
- **4. Authorize Clients to Access a Network**: To authorize clients to access the VPC in which the associated subnet is located, you must create an authorization rule. The authorization rule specifies which clients have access to the VPC. In this tutorial, we grant access to all users. 
- **5. (Optional) Enable Access to Additional Networks**: You can enable access to additional networks connected to the VPC, such as AWS services, peered VPCs, and on-premises networks. For each additional network, you must add a route to the network and configure an authorization rule to give clients access. 
- **6. Download the Client VPN Endpoint Configuration File**: The final step is to download and prepare the Client VPN endpoint configuration file. The configuration file includes the Client VPN endpoint and certificate information required to establish a VPN connection. You must provide this file to the clients who need to connect to the Client VPN endpoint to establish a VPN connection. The client uploads this file into their VPN client application.  
- **7. Check VPN Connection**

## Architecture

<p align = "center">
      <img src = "IMAGES/000-Client-VPN-Connection-Architecture-01.jpg" width = "80%" height = "80%">
      </p>



## Prerequisites

- An AWS account
- Download [OpenVPN GUI](http://openvpn.se/download.html)  (for Windows) and install it
- Download [aws-self-signed-acm](aws-self-signed-acm)
- Putty for login to your EC2 (Windows only)

## Step by Step

### Generate Server and Client Certificates and Keys

1. Create a VPC environment, and lauch a EC2-Linux-Instance in it. Please refer [Connect to EC2 Linux Instance](https://github.com/ecloudvalley/Connect-to-EC2-Linux-Instance). Also create a VPC environment on it. Please refer [Amazon Virtual Private Cloud](https://gitlab.com/ecloudture/knowledge-base/aws-sop/amazon-virtual-private-cloud)

2. After you login into **EC2**, type:

- ``sudo su``

3. Open the file ``aws-self-signed-acm.sh`` which you download and copy all of the command. Then type ``vim cert.sh``. Press **I** on your keyboard and add the information in it.


After you finish setting the information above, press **esc** and type ``:wq!`` and press **enter** to quit.



4. Turn the file into an executable file, then type ``chmod +x cert.sh``.

5. Execute the file then type ``./cert.sh`` and check the file **cert.sh** and the folder **tls** by typing ``ls`` .

6. Type ``cd tls/`` to the folder.

7. Then type ``cat cloudelligent.com.crt``, there will a series of strings appear.

8. On the services menu, select **Certificate Manager**. Then choose **Import a certificate**.

9. In **Step1: Import certificate**: Copy the series of strings see as below and paste it to the space of **Certificate body**.

<p align = "center">
      <img src = "IMAGES/001-string-certificate-body-01.jpg" width = "80%" height = "80%">
      </p>

<p align = "center">
      <img src = "IMAGES/001-console-certificate-body-02.jpg" width = "80%" height = "80%">
      </p>


10. Back to the EC2 instance, then type ``cat cloudelligent.com.key``, a series of strings appear again.

11. Copy the series of strings see as below and paste it to the space of **Certificate private key**.

<p align = "center">
      <img src = "IMAGES/001-string-certificate-private-key-03.jpg" width = "80%" height = "80%">
      </p>

<p align = "center">
      <img src = "IMAGES/001-console-certificate-private-key-04.jpg" width = "80%" height = "80%">
      </p>


12. Back to the EC2 instance again, then type ``cat CA.crt``, appear a series of strings for sure.

13. Copy the series of strings see as below and paste it to the space of  **Certificate chain**.

<p align = "center">
      <img src = "IMAGES/001-string-certificate-chain-05.jpg" width = "80%" height = "80%">
      </p>

<p align = "center">
      <img src = "IMAGES/001-console-certificate-chain-06.jpg" width = "80%" height = "80%">
      </p>


14. After complete it, then click **Review and import**.

15. After review it, remember back up all of your certification. Then click **Import**. In **Certificates** panel, you will check the status is **ISSUED**.




### Setup Client VPN Endpoint

1. On the services menu, select **VPC**.

2. On the left navigation panel, select **Client VPN Endpoints**, then choose **Create Client VPN Endpoint**.

3. Input the following information to **create client VPN endpoint**.

- **Name tag**: ``Customer-Site``
- **Description**: ``Cloudeelligent.com-vpn``
- **IPv4 CIDR block**: ``10.23.0.0/22``

At **Authentication Information** Part:

- **Server certificate ARN**: ``Choose yours``
- **Authentication Options**: Click **Use mutual authentication**
- **Client certificate ARN**: ``Choose yours``

At **Connection Logging** Part:
- **Do you want to log the details on client connections?**: Click ``NO``

At **Other optional parameters** Part:
- **DNS Server 1 IP address**: ``8.8.8.8``
- **DNS Server 2 IP adress**: ``1.1.1.1``
- **Transport Protocol**: Choose **UDP**

After set up, click **Create Client VPN Endpoint**. Then you will find out the status of the endpoint which you create is **Pending-associate**

<p align = "center">
      <img src = "IMAGES/002-pending-associate-01.jpg" width = "80%" height = "80%">
      </p>


4. To enable clients to establish a VPN session, you must associate a target network with the Client VPN endpoint. A target network is a subnet in a VPC. On the bottom navigation panel, select **Associatios**, then click **Associate**.

<p align = "center">
      <img src = "IMAGES/002-associate-subnet-02.jpg" width = "80%" height = "80%">
      </p>

In **Create Client VPN Association to Target Network** part:

- **VPC**: ``the VPC you created``
- **Choose a subnet to associate**: ``the Subnet you created``

Then click **Associate**

<p align = "center">
      <img src = "IMAGES/002-choose-subnet-03.jpg" width = "80%" height = "80%">
      </p>

5. Back to the services menu, select **EC2**. 

6. On the left navigation panel, select **Security Groups**, then choose **Create Security Group**. Enter the following information:

- **Security group name**: ``aws-client-vpn``
- **Description**: ``aws-client-vpn``
- **VPC**: ``default``
On the bottom navigation panel, select **Inbound**, then click **Add Rule**.
Enter the following information:
- **Type**: ``Custom UDP Rule``
- **Port Range**: ``1194``
- **Source**: ``0.0.0.0/0``
then click **Save**

7. Back to the service, select **VPC**. On the left navigation panel, select **Client VPN Endpoints**. On the bottom navigation panel, select **Security Groups**, then click **Apply Security Groups**. Select **aws-client-vpn** you just made, then click **Apply Security Groups**.

8. On the bottom navigation panel, select **Authorization**, then click **Authorize Ingress**. At **Add authorization rule** part, enter the following information:

- **Destination network to enable**: ``0.0.0.0/0``
- **Description**: ``any-where``

Then click **Add authorization rule**. Seconds later, the state will change from **authorizing** to  **active**

9. On the bottom navigation panel, select **Route Table**, then click **Create Route**. At **Create Route** part, enter the following information:

- **Route destination**: ``0.0.0.0/0``
- **Target VPC Subnet ID**: ``the Subnet you created``
- **Description**: ``internet-access``

then click **Create Route**. Seconds later, check the state is **Active**.


### Download the Client VPN Endpoint Configuration File

The final step is to download and prepare the Client VPN endpoint configuration file. The configuration file includes the Client VPN endpoint and certificate information required to establish a VPN connection. You must provide this file to the clients who need to connect to the Client VPN endpoint to establish a VPN connection. The client uploads this file into their VPN client application.

1. On the bottom navigation panel, Select the **Client VPN endpoint** for which to download the Client VPN configuration file and choose **Download Client Configuration**. Then click **Download** and save the file.

<p align = "center">
      <img src = "IMAGES/003-download-client-configuration-01.jpg" width = "80%" height = "80%">
      </p>

2. Open the file by **Notepad**. Insert ``<cert>``, ``</cert>`` , ``<key>`` and ``</key>`` in it.

<p align = "center">
      <img src = "IMAGES/003-insert-cert-key-02.jpg" width = "80%" height = "80%">
      </p>

3. Open the back up of your certification, then copy the strings which you paste in the space of **Certificate body**. Under ``<cert>`` , paste the strings on it.

<p align = "center">
      <img src = "IMAGES/003-insert-cert-03.jpg" width = "80%" height = "80%">
      </p>

4. Open the back up of your certification, then copy the strings which you paste in the space of  **Certificate private key**. Under ``<key>`` , paste the strings on it.


<p align = "center">
      <img src = "IMAGES/003-insert-key-04.jpg" width = "80%" height = "80%">
      </p>


5. **Save** the file.


### Check the Connection

1. Open **OpenVPN GUI**, click **import file**. Then choose ``downloaded-client-config``, and click **connect**. Seconds later, it appears **File imported successfully**.

<p align = "center">
      <img src = "IMAGES/004-openvpn-import-file-00.jpg" width = "80%" height = "80%">
      </p>


2. It shows up **already connect to downloaded-client-config**.

<p align = "center">
      <img src = "IMAGES/004-openvpn-connect-01.jpg" width = "80%" height = "80%">
      </p>


3. Back the services menu, select **VPC**.

4. On the left navigation panel, select **Client VPN Endpoints**. Then choose the VPN endpoint you create. 

5. On the bottom navigation panel, select **Connections** . Check the status in the customer site. It should be **Active**.

<p align = "center">
      <img src = "IMAGES/004-consle-connection-02.jpg" width = "80%" height = "80%">
      </p>


<p align = "center">
      <img src = "IMAGES/004-consle-connection-03.jpg" width = "80%" height = "80%">
      </p>


## Conclusion

Congratulations! You have learned how to :

- Generate server and client certificates and keys

- Create client VPN endpoint

- How to enable VPN connectivity for end users and end user access to workloads.

- Enable network connectivity to access other networks and check connection


## Cleanup

- Certificates 
- EC2 instances
- VPN endpoints
- VPC
